
import * as AuthAction from './auth.actions';

export interface IAuthState {
    token: string;
    authenticated: boolean;
}

const initialState: IAuthState = {
    token: null,
    authenticated: false
};

export function authReducer(state = initialState, action: AuthAction.AuthActions ) {
    switch (action.type) {
        case AuthAction.SIGNIN :
        case AuthAction.SIGNUP:
        return {
            ...state,
            authenticated: true
        };
        case AuthAction.LOGOUT :
        return {
            ...state,
            token : null,
            authenticated: false
        };

        case AuthAction.SET_TOKEN:
        return {
            ...state,
            token: action.payload,
            authenticated: true
        };
        default:
        return state;
    }

}
