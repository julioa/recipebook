import * as fromShoppingList from '../shopping-list/store/shoping-list.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import { ActionReducerMap } from '@ngrx/store';

export interface IAppState {
    shoppingList: fromShoppingList.IShoppingListState,
    auth: fromAuth.IAuthState
}

export const reducers: ActionReducerMap<IAppState> = {
    shoppingList: fromShoppingList.shoppingListReducer,
    auth: fromAuth.authReducer
}