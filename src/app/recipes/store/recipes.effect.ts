import { Actions, Effect, ofType } from '@ngrx/effects';
import * as recipeActions from '../../recipes/store/recipes.actions';
import * as recipeReducer from '../../recipes/store/recipes.reducer';
import { Injectable } from '@angular/core';
import { switchMap, map, catchError, withLatestFrom } from 'rxjs/operators';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { Recipe } from '../recipe.model';
import { throwError } from 'rxjs';
import { Store } from '@ngrx/store';


@Injectable()
export class RecipeEffects {

    constructor(private actions$: Actions, private httpclient: HttpClient, private store: Store<recipeReducer.IRecipeFeature>) {}
    @Effect()
    fetchRecipe = this.actions$.
    pipe( ofType(recipeActions.FETCH_RECIPES),
    switchMap((action: recipeActions.FetchRecipes) => {
         return this.httpclient.get<Recipe[]>('https://ng-recipe-book-49163.firebaseio.com/data.json');
    }),
    map(
        (recipes) => {
            for (let recipe of recipes) {
                if (!recipe['ingredients']) {
                    recipe['ingredients'] = [];
                }
            }
            return  {
                type: recipeActions.SET_RECIPES,
                payload: recipes
            };
        }
    )
  );

  @Effect()
  storeRecipe = this.actions$.
  pipe(
      ofType(recipeActions.STORE_RECIPES),
      withLatestFrom( this.store.select('recipes')),
      switchMap(
          ([action, state]) => {
            const req = new HttpRequest(
                'PUT',
                'https://ng-recipe-book-49163.firebaseio.com/data.json'
                , state.recipes,
                {
                 reportProgress: true,
                }
            );
            return this.httpclient.request(req);
          }
      )
   );
}
