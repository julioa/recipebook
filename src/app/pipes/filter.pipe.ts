import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customFilter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string): any {
    let lowercaseValue: string=null;
    if(value.length===0 || filterString==='' || filterString===undefined){
      return value;
    }
    let items=[];
    lowercaseValue = filterString.toLowerCase();
  
    for(let item of value){  
      let result = item[propName].split(' ').some(itemSearch=>{
        let tmp:string = itemSearch.toLowerCase();
         if(tmp===lowercaseValue){
          items.push(item)
         }
        });
    }
   return items;
  
  
  }

}
