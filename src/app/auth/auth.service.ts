import * as firebase from 'firebase';
import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as Actions from '../auth/store/auth.actions';

@Injectable()
export class AuthService {

constructor(
    private router: Router, 
    private route:ActivatedRoute,
    private store: Store<fromApp.IAppState>)
{

}
 token: string;
 registerUser(email:string, password:string){
    
  firebase.auth().createUserWithEmailAndPassword(email, password)
  .then(
      user=> {
          this.store.dispatch(new Actions.SignUp());
      }
  )
  .catch(
      (error)=>{
          console.log('error');
      }
  )
 }

 signInUser(email:string, password:string){
     firebase.auth().signInWithEmailAndPassword( email, password )
     .then(
         response=> { 
            this.store.dispatch(new Actions.SignUp())
            this.router.navigate(['/']);
            firebase.auth().currentUser.getIdToken().then(
                (jwtoken:string)=>{
                    this.store.dispatch( new Actions.SetToken(jwtoken));
                }
            ).catch(
                error=> {
                    console.log(error);
                }
            )
         }
     )
     .catch(
         error=>{
             console.log(error);
         }
     )
 }
 getToken(){
     firebase.auth().currentUser.getIdToken()
     .then( 
         (jwtoken)=> {
           this.token = jwtoken;
         }
     )
     .catch(
         error=> { 
             console.log('error');
         }
     );

     return this.token;
 }

 isAuthentificaded(){
     return this.token!=null;
 }
 logOut(){
     
     firebase.auth().signOut();
     this.token  = null;
    // this.router.navigate(['login'], { relativeTo : this.route })
 }

}