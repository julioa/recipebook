import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as Actions from '../store/shopping-list.actions';
import * as fromApp from '../../store/app.reducer';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  
  @ViewChild('f') shoppingListForm: NgForm;
  subscriptionEdit: Subscription;
  indexToEdit: number;
  editMode: boolean= false
  ingredientEdited: Ingredient;

  constructor(private store: Store<fromApp.IAppState>) { }

  ngOnInit() {
       
    this.subscriptionEdit =  this.store.select('shoppingList')
    .subscribe(
      data=>{
        if(data.editedIngredientIndex > -1){
          this.ingredientEdited= data.editedIngredient;
          this.editMode =  true;
          this.shoppingListForm.setValue({
            name: this.ingredientEdited.name,
            amount: this.ingredientEdited.amount
          })
        }else {
          this.ingredientEdited = null;
           this.editMode = false;
        }
      }
    )


   
  }
  ngOnDestroy()
  {
    this.subscriptionEdit.unsubscribe();  
    this.store.dispatch( new Actions.StopEdit());
  }

  onSubmit(form:NgForm){

    let value = form.value;
    let newIngredient = new Ingredient( value.name, value.amount );    
     
    if(this.editMode){
      this.store.dispatch( new Actions.UpdateIngredient(newIngredient))

    }else{
       this.store.dispatch(new Actions.AddIngredient( newIngredient ) );
    }
    this.store.dispatch( new Actions.StopEdit());
    form.reset();
    this.editMode = false;
    
  }
  onClear(){
    this.shoppingListForm.reset();
    this.editMode =false;
  }
  onDelete(){

    this.shoppingListForm.reset();
    this.editMode = false;
    this.store.dispatch( new Actions.DeleteIngredient());
  }

}
