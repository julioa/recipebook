import { Component, OnInit, ViewEncapsulation, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import * as fromrecipeListReducer from '../store/recipes.reducer';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css'],
 //providers : [ RecipeService ]
  /*encapsulation: ViewEncapsulation.None*/
})
export class RecipeListComponent implements OnInit  {
   subscription: Subscription;
   subscriptionAsync: Subscription;
   recipesState: Observable<fromrecipeListReducer.IRecipeState>;
   asyncRequest = false;
   searchValue;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromrecipeListReducer.IRecipeFeature>) { }

  ngOnInit() {
    this.recipesState = this.store.select('recipes');
  }
  onAddRecipe() {
   this.router.navigate(['new'], { relativeTo: this.route });
  }


}
