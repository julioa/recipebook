import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import * as AuthActions from './store/auth.actions';
import { map, switchMap, mergeMap, tap } from 'rxjs/operators';
import * as firebase from 'firebase';
import { from } from 'rxjs';
import { Router } from '@angular/router';



export interface IUser {
    username: string;
    password: string;
}

@Injectable()
export class AuthEffects {

    constructor(private actions$: Actions, private router: Router) {}

    @Effect()
    authSignUp =  this.actions$
    .pipe( ofType(AuthActions.TRY_SIGNUP),
           map   ( (action: AuthActions.TrySignUp) => {
                    return action.payload;
                 }),
          switchMap( (authData: IUser) => {
                   return  from( firebase.auth().createUserWithEmailAndPassword( authData.username, authData.password));
                   }),
          switchMap(
                 () => {
                  return from(firebase.auth().currentUser.getIdToken());
                   }),
         mergeMap(
                   (token: string) => {
                       return [
                           {
                               type: AuthActions.SIGNUP
                           },
                           {
                               type: AuthActions.SET_TOKEN,
                               payload: token
                           }
                       ];
                   }
                 )

       );


       // SIGN IN EFFECT
       @Effect()
       authSignIn = this.actions$
       .pipe(
           ofType(AuthActions.TRY_SIGNIN),
           map( (action: AuthActions.TrySignIn) => {
                return action.payload;
           }),
           switchMap(
               (authData: IUser) => {
                   // firebase authentification
                   return from(  firebase.auth().signInWithEmailAndPassword( authData.username, authData.password ) )
               }
           ),
           switchMap(
               () => {
                   return from(firebase.auth().currentUser.getIdToken())
               }
           ),
           mergeMap(
               (token: string) => {
                this.router.navigate(['/']);
                return[
                       {
                           type: AuthActions.SIGNIN
                       },
                       {
                           type: AuthActions.SET_TOKEN,
                           payload: token
                       }
                   ];
               }
           )
       );

       // LOGOUT
       @Effect({ dispatch: false})
       authLogout = this.actions$
       .pipe(
           ofType(AuthActions.LOGOUT),
           tap( () => {
               this.router.navigate(['/']);
           })
       );
}
