import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp  from '../store/app.reducer';
import * as Actions from './store/shopping-list.actions';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  subscription: Subscription;
  shoppingListState: Observable<{ ingredients: Ingredient[] }>;
  constructor(
          private store: Store<fromApp.IAppState>) { }

  ngOnInit() {
   this.shoppingListState = this.store.select('shoppingList');
  }

  onEditIngredient(id: number) {
    this.store.dispatch(new Actions.StartEdit(id));
  }

}
