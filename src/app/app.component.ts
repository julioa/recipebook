import { Component, OnInit } from '@angular/core';
import { LoggingService } from './shared/logging.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ LoggingService ]
})
export class AppComponent implements OnInit {

  constructor(private logging: LoggingService){

  }
  
  ngOnInit(){
      firebase.initializeApp({
        apiKey: "AIzaSyCST9w8675ojNjmHV3lPg7kcbF6F5hixhA",
        authDomain: "ng-recipe-book-49163.firebaseapp.com"
      })
  }

  linkSelected: string='Recipes';
  title = 'my-first-project';

 

}
