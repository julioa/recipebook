import { Component, OnInit  } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import * as Actions from '../../shopping-list/store/shopping-list.actions';
import * as fromRecipeReducer from '../../recipes/store/recipes.reducer';
import { Observable } from 'rxjs';
import {  take } from 'rxjs/operators';
import * as fromRecipeAction from '../store/recipes.actions';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
 recipeState: Observable<fromRecipeReducer.IRecipeState> ;
 id: number;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromRecipeReducer.IRecipeFeature> ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (param: Params) => {
       this.id = + param.id;
       this.recipeState = this.store.select('recipes');
      }
    );
  }
  onAddToShoppingList() {
    this.store.select('recipes')
    .pipe(
      take(1)).subscribe( (recipeState: fromRecipeReducer.IRecipeState) => {
       return this.store.dispatch( new Actions.AddIngredients(recipeState.recipes[this.id].ingredients));
      });
  }
  onDelete() {
  this.store.dispatch( new fromRecipeAction.DeleteRecipe(this.id));
  this.router.navigate(['/recipes']);
  }

}
