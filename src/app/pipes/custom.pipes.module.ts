import { NgModule } from '@angular/core';
import { FilterPipe } from './filter.pipe';
import { CapitalizePipe } from './capitalize.pipe';
import { CustomSortPipe } from './custom-sort.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations:[
        FilterPipe,
        CapitalizePipe,
        CustomSortPipe
    ],
    exports : [
        CommonModule,
        FilterPipe,
        CapitalizePipe,
        CustomSortPipe
    ]
})
export class CustomPipesModule {

}