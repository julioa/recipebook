import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import { switchMap, take } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    constructor(
        private authService:AuthService,
        private store: Store<fromApp.IAppState>
        ){

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('intercepted', req);
        return this.store.select('auth')
                            
               .pipe(
                   take(1),
                   switchMap(
                       (authState: fromAuth.IAuthState)=>{
                            const copiedReq = req.clone({
                                params: req.params.set('auth', authState.token )
                            });
                        return next.handle(copiedReq);
                       }
                    )
               )
      /*const copiedReq = req.clone({
            params: req.params.set('auth',this.authService.getToken())
        });
      return next.handle(copiedReq);*/
    }

} 