import { Directive, OnInit, Input, ElementRef, HostBinding, HostListener } from '@angular/core';
@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {
   
    @HostBinding('class.open') isOpen=false;
    
    //listen events
    @HostListener('click') toogleOpen(){
      this.isOpen = !this.isOpen;
    }

    constructor(){

    }
    ngOnInit(){

    }

}