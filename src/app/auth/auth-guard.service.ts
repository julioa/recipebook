import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';
import * as fromAuth from '../auth/store/auth.reducer';
import { map, take } from 'rxjs/operators';

@Injectable()
export class AuthGuardService implements CanActivate {
   constructor(
       private store: Store<fromApp.IAppState>
       ) {}
   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.
            select('auth').
            pipe(
                take(1),
                map(
                        (authState: fromAuth.IAuthState) => {
                          return authState.authenticated;
                        }
                    )
                );
   }
}
