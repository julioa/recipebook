import { Recipe } from '../recipe.model';
import { Ingredient } from 'src/app/shared/ingredient.model';
import * as fromRecipeActions from '../store/recipes.actions';
import * as fromApp from '../../store/app.reducer';

export interface IRecipeFeature extends fromApp.IAppState {
  recipes: IRecipeState;
}


export interface IRecipeState {
  recipes: Recipe[];
}

const initialState: IRecipeState = {
      recipes: [
        new Recipe(
          'how to create a tasty salad',
          'this is a simply test',
          'https://i.dietdoctor.com/wp-content/uploads/2018/03/DD-599-ketocobbsalad-2.jpg',
           [
               new Ingredient('Apple', 1),
               new Ingredient('onion', 2)
           ]
          ),
          new Recipe(
              'make a easy cake in 10 minutes',
              'this is a simply test',
              'https://www.bbcgoodfood.com/sites/default/files/styles/recipe/public/recipe/recipe-image/2018/02/easter-nest-cake.jpg',
               [
                   new Ingredient('Pepper', 1),
                   new Ingredient('Grapes', 2)
               ]
              )
      ]
};

export function RecipeReducer(state = initialState, action: fromRecipeActions.RecipeActions) {
  switch ( action.type ) {
    case fromRecipeActions.SET_RECIPES:
      return {
      ...state,
      recipes: [...action.payload]
      };
    case fromRecipeActions.ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload ]
      };
    case fromRecipeActions.UPDATE_RECIPE:
      const recipeList = [...state.recipes];
      // search the recipe
      const recipe =  recipeList[action.payload.index];

      const updatedRecipe = {
          ...recipe,
          ...action.payload.updatedRecipe
      };
      recipeList[action.payload.index] = updatedRecipe;
      return {
        ...state,
        recipes: recipeList
      };
    case fromRecipeActions.DELETE_RECIPE:
      const oldlist = [...state.recipes];
      oldlist.splice(action.payload, 1);
      return {
        ...state,
        recipes: oldlist
      };
    default :
    return state;
  }
  
}
