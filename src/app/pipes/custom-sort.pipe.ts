import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customSort'
})
export class CustomSortPipe implements PipeTransform {

  transform(value: any, propName: string): any {
    
    if(value.length===0 || propName===undefined){
      propName = 'name';
    }
    
    let tmpvalues:[]  = value;

    return tmpvalues.sort( (a,b)=> {
      if(a[propName]>b[propName]){
        return 1;
      }
      if(a[propName]<b[propName]){
        return -1;
      }
    })

  }

}
