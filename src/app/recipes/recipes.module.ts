
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RecipeDetailComponent } from './recipe-detail/recipe-detail.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { StartRecipesComponent } from './start-recipes/start-recipes.component';
import { RecipesComponent } from './recipes.component';
import { RecipeItemComponent } from './recipe-list/recipe-item/recipe-item.component';

import { RecipeRoutingModule } from './recipe.routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { CustomPipesModule } from '../pipes/custom.pipes.module';
import { StoreModule } from '@ngrx/store';
import { RecipeReducer } from './store/recipes.reducer';
import { EffectsModule } from '@ngrx/effects';
import { RecipeEffects } from './store/recipes.effect';



@NgModule({
    declarations: [
        RecipeDetailComponent,
        RecipeEditComponent,
        RecipeListComponent,
        StartRecipesComponent,
        RecipesComponent,
        RecipeItemComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        FormsModule,
        RecipeRoutingModule,
        SharedModule,
        CustomPipesModule,
        StoreModule.forFeature('recipes', RecipeReducer),
        EffectsModule.forFeature([RecipeEffects])
    ]
})
export class RecipesModule {

}