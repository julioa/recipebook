const express  = require('express');
const app = express();
const path = require('path');
app.use(express.static(__dirname + '/dist/my-first-project'));

app.listen(process.env.PORT || 8080);


//pathlocationStrategy
app.get('/*', (req,res)=>{
    res.sendFile( path.join(__dirname+'/dist/my-first-project/index.html')); 
});

console.log('Listening');
