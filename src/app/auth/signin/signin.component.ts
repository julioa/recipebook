import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import * as AuthActions from '../store/auth.actions';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  user: {
    email: null,
    password: null
 };
  constructor(private store: Store<fromApp.IAppState>) { }

  ngOnInit() {
  }
  onSignin(form: NgForm) {
    this.user = {
      email : form.value.email,
      password: form.value.password
    };
    this.store.dispatch( new AuthActions.TrySignIn({ username: this.user.email, password: this.user.password } ) );
  }
}

