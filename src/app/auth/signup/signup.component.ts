import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducer';
import * as authActions from '../../auth/store/auth.actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  user: {
     email : null,
     password: null
  }
  constructor( private store: Store<fromApp.IAppState>) { }

  ngOnInit() {
  }
  onSignUp(form: NgForm){
    this.user = {
      email : form.value.email,
      password:form.value.password
    }
    
    this.store.dispatch( new authActions.TrySignUp({ username : this.user.email, password : this.user.password }))
    
   // this.authService.registerUser( this.user.email, this.user.password );

  }
}
