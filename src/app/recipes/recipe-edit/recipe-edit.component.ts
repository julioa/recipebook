import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormGroupName, Validators } from '@angular/forms';

import * as fromRecipe from '../store/recipes.reducer';
import * as fromRecipeActions from '../store/recipes.actions';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  editMode = false;
  recipeForm: FormGroup;
  idToEdit: number;


  constructor(private router: Router,
              private route: ActivatedRoute,
              private store: Store<fromRecipe.IRecipeFeature>) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        console.log('param' + params.id);
        this.editMode = params.id != null;
        this.idToEdit = params.id;
        this.onInitForm();
      }
    );
  }
  onDeleteIngredient(id: number) {
   (<FormArray>this.recipeForm.get('ingredients')).removeAt(id);
  }
  onSubmit() {
     if (this.editMode) {
       this.store.dispatch(new fromRecipeActions.UpdateRecipe({ index: this.idToEdit, updatedRecipe: this.recipeForm.value }));
     } else {
       this.store.dispatch(new fromRecipeActions.AddRecipe(this.recipeForm.value));
     }
     this.onCancel();
  }

  onInitForm() {
    let recipeName: string = null;
    let recipeDescription: string = null;
    let recipeImage: string = null;
    let recipeIngredients = new FormArray([]);
    if (this.editMode) {
      this.store.select('recipes')
      .pipe(
        take(1),
      ).subscribe(
        (recipeState: fromRecipe.IRecipeState) => {
          const recipeInfo = recipeState.recipes[this.idToEdit];
          recipeName = recipeInfo.name;
          recipeDescription = recipeInfo.description;
          recipeImage = recipeInfo.imagePath;
          if (recipeInfo['ingredients']) {
            for (const ingredient of recipeInfo.ingredients) {
              recipeIngredients.push(
                new FormGroup({
                  name: new FormControl(ingredient.name, Validators.required),
                  amount: new FormControl(ingredient.amount, [Validators.required, Validators.pattern('^[1-9]+[0-9]*$') ])
                })
              );
            }
          }
        }
      );
    }

    this.recipeForm = new FormGroup({
      name: new FormControl(recipeName, Validators.required),
      imagePath: new FormControl(recipeImage, Validators.required),
      description: new FormControl(recipeDescription,Validators.required),
      ingredients: recipeIngredients
    });
  }
  getIngredientsCtrl() {
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }
  onCancel() {
   this.router.navigate(['../'], { relativeTo: this.route });
  }

  onAddIngredient() {
    const formGroupIngredient = new FormGroup({
        name : new FormControl(null, Validators.required),
        amount: new FormControl(null, [Validators.required, Validators.pattern('^[1-9]+[0-9]*$')])
    });
    (<FormArray> this.recipeForm.get('ingredients')).push( formGroupIngredient );
  }



}
