import { Ingredient } from '../../shared/ingredient.model';
import  * as Actions  from  './shopping-list.actions';

//shopping list state
export interface IShoppingListState {
    ingredients : Ingredient[],
    editedIngredient: Ingredient,
    editedIngredientIndex: number
}


const initialState: IShoppingListState = {
    ingredients : [
        new  Ingredient ('Apples',5),
        new Ingredient('Tomatoes',7)
    ],
    editedIngredient: null,
    editedIngredientIndex: -1
};
   

export function shoppingListReducer(state = initialState, action: Actions.ShoppingListActions  ){
    switch(action.type) {
        case Actions.ADD_INGREDIENT :
        return {
            ...state,
            ingredients:[...state.ingredients,action.payload]
        }
        case Actions.ADD_INGREDIENTS:
            return {
                ...state,
                ingredients:[...state.ingredients,...action.payload]
            }
        case Actions.UPDATE_INGREDIENT:
            let ingredient = state.ingredients[state.editedIngredientIndex];
            let updatedIngredient = {
                ...ingredient,
                ...action.payload
            };
            let ingredients = [...state.ingredients];
            ingredients[state.editedIngredientIndex] = updatedIngredient; 

            return {
                ...state,
                ingredients: ingredients
            }
        case Actions.DELETE_INGREDIENT:
            const oldIngredients = [...state.ingredients];
            oldIngredients.splice(state.editedIngredientIndex,1);
            return {
                ...state,
                ingredients : oldIngredients
            }

        case Actions.START_EDIT:
        const editedIngredient = {...state.ingredients[action.payload]};
        return {
            ...state,
            editedIngredient: editedIngredient,
            editedIngredientIndex: action.payload
        }
        case Actions.STOP_EDIT :
        return {
            ...state,
            editedIngredient: null,
            editedIngredientIndex: -1
        }

        default:
        return state;
    }
  
}