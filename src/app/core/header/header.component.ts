import { Component, OnInit} from '@angular/core';

import { Recipe } from '../../recipes/recipe.model';
import { Observable } from 'rxjs';
import * as fromAuth from '../../auth/store/auth.reducer';
import * as fromApp from '../../store/app.reducer';
import * as fromRecipeActions from '../../recipes/store/recipes.actions';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['header.component.css']
})

export class HeaderComponent implements OnInit {
    success = false;
    error: string = null;
    recipes: Recipe[];
    authState: Observable<fromAuth.IAuthState>;
    constructor(
        private store: Store<fromApp.IAppState>
        ) {
    }
    ngOnInit() {
        this.authState = this.store.select('auth');
    }
    onSave() {
       this.store.dispatch(new fromRecipeActions.StoreRecipes());
    }
    onFetchData() {
        this.store.dispatch(new fromRecipeActions.FetchRecipes());
    }
}
